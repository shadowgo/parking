let login = "phone";
let map;

let socket = io("http://localhost:8097/public")

let parkings = []


socket.on('connect', (data)=>{
    socket.on('auth_data', (data)=>{
        if(data.token) {
            localStorage.token = data.token
            socket.emit('auth', localStorage.token)
        }
    })

    const ti = setInterval(() => {
        if( localStorage.token) {
            socket.emit('parkings', null)
            clearInterval(ti)
        }
    }, 3000);

    

    socket.on('alert', (data)=>{
        alert(data)
    })
})

function switchView(to = '') {
    document.querySelector('div.view.show').classList.remove('show');
    document.querySelector(`div.view.${to}`).classList.add('show');
}

function loginSubmit() {
    if (login == "phone") {

        socket.emit('login', {
            phone : document.getElementById("phoneInput").value 
        })
        let container = document.querySelectorAll('div.view.login div.input-container');
        container[0].classList.add('hide');
        container[1].classList.remove('hide');
        document.getElementById('login-text').innerText = 'کد تایید ۵ رقمی به شماره موبایل شما ارسال شد.';
        document.getElementById('login-button').innerText = 'تایید';
        login = "token";
    } else if (login == "token") {
        if(!document.getElementById("codeInput").value ){
            return
        }

        socket.emit('login', {
            phone : document.getElementById("phoneInput").value,
            code : document.getElementById("codeInput").value
        })
        document.getElementById('login-text').classList.add('hide');
        document.querySelectorAll('div.view.login div.input-container')[1].classList.add('hide');
        document.getElementById('more').classList.remove('hide');
        document.getElementById('login-button').innerText = 'ثبت';
        login = "submit";
    } else if (login == "submit") {
        mapInit();
    }
}

function mapInit() {
    switchView('home')
    map = new L.Map('map', {
        key: 'web.0amk8kUfdx1JLEBh2aKOrand6sVdBsICD5WMpR1D',
        maptype: 'dreamy',
        poi: true,
        traffic: false,
        center: [35.699739, 51.338097],
        zoom: 14
    });


    socket.on('add_parking', (data)=>{

        if(!data) {
            return
        }
        newMarker({ latitude: data.lat, longitude: data.long}, data._id)




    })
   
}

function newMarker(position = { latitude: 0, longitude: 0 }, id = "") {
    
    let marker = L.marker([position.latitude, position.longitude], { alt: id }).addTo(map)

    marker.on('click', event => {
        let selectedID = event['target']['options']['alt'];
        openParking({ latitude: event['latlng']['lat'], longitude: event['latlng']['lng'] }, selectedID);
    })
}

function openParking(position = { latitude: 0, longitude: 0 }, id = "") {

    console.log(id)
    document.querySelector('div.parks').innerHTML = "";
    document.querySelector('div.parking').classList.add('loading');
    setTimeout(() => {
        document.getElementById('map').classList.add('short');
        document.querySelector('div.parking').classList.remove('loading');
        document.querySelector('div.parking').classList.add('show');
        document.getElementById('price-text').innerText = 'مبلغ توافقی';

        selectFloor();
 
        map.flyTo([position.latitude - 0.005, position.longitude], 16)
    }, 500);
}

function RequestParking() {
    closeParking()

    alert('ثبت درخواست موفق بود')
}
function closeParking() {
    let position = map.getCenter();
    map.flyTo([position['lat'] + 0.005, position['lng']], 14);
    document.getElementById('map').classList.remove('short');
    document.querySelector('div.parking').classList.remove('show');
}


function loadFloor(floor = "0", status = [], poses) {
    function create(number, reversed = false) {
        let div = document.createElement('div');
        div.classList.add('park');
        if (reversed == true) div.classList.add('reversed');
        else div.innerText =  number.toString();
        div.onclick = () => selectPark(number);

        document.querySelector('div.parks').appendChild(div);
    }

    document.querySelector('div.parks').innerHTML = "";

    for (let i = 0; i < poses; i++) {
        create(i, status.includes(i));
    }
    document.getElementById('floor-index').innerText = floor;
}

function selectFloor(action) {
    const limit = 2
    let index = parseInt(document.getElementById('floor-index').innerText);

    

    let array = [];
    if (action == '+') {
        if (index < limit) {
            loadFloor(++index, array, 40);
        }
    } else if (action == '-') {
        if (index > 0) {
            loadFloor(--index, array, 40);
        }
    } else {
        loadFloor(0, array, 40);
    }
}

function selectPark(index) {
    if (document.querySelector('div.parks div.park.click'))
        document.querySelector('div.parks div.park.click').classList.remove('click');
    document.querySelectorAll('div.parks div.park')[index].classList.add('click')
}

window.onload = () => {
    setTimeout(() => {
        if (localStorage.getItem('myCar')){
            document.querySelector('.mycar').classList.remove('hide');
        }

        if(!localStorage.token) {
            switchView('login')
        }else {
            socket.emit('auth', localStorage.token)
            mapInit()
        }
        
    }, 1000);
}