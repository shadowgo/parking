let mongoose = require('mongoose');
var tungus = require('tungus');
let db = mongoose.createConnection(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}?authSource=admin`, 
{useNewUrlParser: true})


var tokens = new mongoose.Schema({
  uid  : String,
  token: String,
  admin: Boolean,
}, { collection: 'tokens' });

var parks = new mongoose.Schema({
  name: String,
  address: String,
  long: Number,
  lat: Number,
  floors: Number,
  positions: Number,
}, { collection: 'parks' });

var bookings = new mongoose.Schema({
  id  : String,
  uid  : String,
  pid: String,
  floor: Number,
  position: Number,
}, { collection: 'bookings' });

var phones = new mongoose.Schema({
  phone : {
    type:String,
    required: true,
    unique: true
  },
  tokenNumber : {
    type:String,
    required: true
  },
  timeout : {
    type:Number,
    required: true
  }
}, { collection: 'phones' });

module.exports = {
  tokens : db.model('tokens', tokens),
  phones : db.model('phones', phones),
  bookings : db.model('bookings', bookings),
  parks : db.model('parks', parks),

};