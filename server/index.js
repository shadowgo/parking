require('dotenv').config()
 // hi kaveh
var Kavenegar = require('kavenegar');
var api = Kavenegar.KavenegarApi({
    apikey: process.env.KAVEH
});
var io = require('socket.io')(8097);
var finalhandler = require('finalhandler')
var http = require('http')
var serveStatic = require('serve-static')
var models = require('./model')
const TokenGenerator = require('uuid-token-generator');
const tokgen = new TokenGenerator(256, TokenGenerator.BASE62);

io.of('public').on('connection', async(socket)=>{


    if(socket.handshake.query.token) {

        try {
            const TokenData = await models.tokens.findOne({
                token : data,
            })

            if(TokenData) {
                socket.user = TokenData

                if(TokenData.admin) {
                    socket.admin = true
                }
            }else {
                socket.emit('alert', 'ورود نا معتبر')
            }
        } catch (error) {
            
            socket.emit('alert', 'ورود نا معتبر')
        }

    }


    socket.on('auth', async (data)=>{
        try {
            const TokenData = await models.tokens.findOne({
                token : data,
            })

            if(TokenData) {
                socket.user = TokenData

                if(TokenData.admin) {
                    socket.admin = true
                }
            }else {
                socket.emit('alert', 'ورود نا معتبر')

            }
        } catch (error) {
            socket.emit('alert', 'ورود نا معتبر')

        }
    })

    socket.on('login', async (data)=>{
        if((data.phone) && (data.code)) {

           
            try {
                const Find = await models.phones.findOne({
                    tokenNumber : data.code,
                    phone : data.phone
                })

                if (Find) {
                    const token = tokgen.generate()

                    const generatedToken = await models.tokens.create({
                        token : token,
                        uid : data.phone,
                        admin : false
                    })

                    socket.emit('auth_data', generatedToken)
                }
            } catch (error) {
                
            }
            
        }else if ((data.phone) && (!data.code)){


            const code = generateOTP()

            const phone = PhoneNumber(data.phone)


 

            try {
                const user = await NewPhone(phone, code)

                api.VerifyLookup({
                    receptor: phone,
                    token: code,
                    template: "parkino"
                }, function (response, status) {})
                console.log(user)
            } catch (error) {
                
            }


        }
    })
    UserEvent(socket, 'parkings', async (data)=>{
        try {
            const parks = await models.parks.find({})


            console.log(parks)
            parks.forEach(element => {
                socket.emit('add_parking', element)
            });

        } catch (error) {

            console.log(error)
            socket.emit('alert', 'هیچ پارکینکی تعریف نشده است')
        }
    })

    UserEvent(socket, 'park', async (data)=>{
        try {
            const park = await models.bookings.create(data)

            socket.emit('park', park)

        } catch (error) {
            socket.emit('alert', 'در این محل پارک نمیگردد')
        }
    })

    UserEvent(socket, 'profile', async (data)=>{
        socket.emit('profile', data)
    })

    UserEvent(socket, 'my_car', async (data)=>{
        try {
            const my_car = await models.bookings.findOne({
                uid : socket.user.uid
            })
            socket.emit('my_car', my_car)

        } catch (error) {
        }
    })




    AdminEvent(socket, 'add_parking', async (data)=>{
        try {
            const park = await models.parks.create(data)

            socket.emit('add_parking', park)

        } catch (error) {
            socket.emit('alert', 'خطا در افزودن پارکینگ')
        }
    })


    AdminEvent(socket, 'release_car', async (data)=>{
        socket.emit('release_car', data)
    })
    
})



function UserEvent(socket, ev, fn) {

    socket.on(ev, (data)=>{

        if(socket.user) {

            fn(data)
        }

    })
    

}

function AdminEvent(socket, ev, fn) {

    socket.on(ev, (data)=>{

        if(socket.admin) {

            fn(data)
        }

    })
    

}


const generateOTP = function () { 
          
    // Declare a digits variable  
    // which stores all digits 
    var digits = '0123456789'; 
    let OTP = ''; 
    for (let i = 0; i < 6; i++ ) { 
        OTP += digits[Math.floor(Math.random() * 10)]; 
    } 

    return OTP; 
} 


const PhoneNumber = function(phone) {

    if(phone.length == 10) {
 
         return '0' + phone
     }
     if(phone.length < 11) {
 
         return false
     }else {
       if(phone.startsWith('98')){
        phone = phone.slice(2);
       }else if(phone.startsWith('+98')){
        phone = this.phone.slice(3);
       }
       if(phone.length == 10) {
        phone = '0' + phone
       }
       
       if (phone.length > 11) {
       
               return false  
       }
       return phone
     }
 }


 const NewPhone = async function(phone, otp , timeout = 120000) {


    try {
        const m_phone = await  models.phones.findOne({ phone : phone})
        const otpm = otp
        if (!m_phone) {
            return models.phones.create({

                phone : phone,
                tokenNumber : otpm,
                timeout : timeout
            })
        }else {
            return models.phones.updateOne({

                phone : phone
            }, {
                $set : {
                    tokenNumber : otpm,
                    timeout : timeout
                }
            }).exec()
        }
    } catch (error) {
        return error
    }


    

}



// Serve up public/ftp folder
var serve = serveStatic('../client', { 'index': ['index.html', 'index.htm'] })

// Create server
var server = http.createServer(function onRequest (req, res) {
  serve(req, res, finalhandler(req, res))
})

// Listen
server.listen(3007)


console.log(`
      hello client in port 3007
`)